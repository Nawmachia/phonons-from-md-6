#!/usr/bin/env python3
################################################################
#
# Analyse CASTEP md trajectory file to extract phonon info
#
################################################################

import argparse
import sys
from typing import List

import numpy as np
import scipy.constants as sp_c
import scipy.signal
from scipy.fft import fft
import matplotlib.pyplot as plt

def parse_cli_flags(argv: List[str]) -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        description="Analyse a CASTEP molecular dynamics trajectory file to extract phonon information",
        epilog="Example usage: ./md_to_phonons -o configuration.out mdinput.md",
    )

    parser.add_argument(
        "md_file", type=str, nargs="?",
        help="CASTEP molecular dynamics trajectory file",
    )

    parser.add_argument(
        "-o", "--output",type=str, required=False,
        help="Output file to write, Default is to standard output",
        default="-",
    )

    parser.add_argument(
        "-d", "--discard", type = int, required=False,
        help = "Discard n number of points from beginning of md file",
        default = 0
    )

    parser.add_argument(
        "-T", "--temperature", type = str, required=False,
        help = "Output temperature data file and graph ",
    )

    parser.add_argument(
        "-cv", "--c_v", type = str, required=False,
        help = "Output c_v data file and graph ",
    )

    parser.add_argument(
        "-cwt", "--c_w_tot", type = str, required=False,
        help = "Output c_w_tot data file and graph ",
    )

    return parser.parse_args(argv)


def main(argv: List[str]) -> None:
    import castep_md_reader as md

    arguments = parse_cli_flags(argv)    #Use this version of parse
    input_filename  = arguments.md_file  #not the one in castep_md_reader\
    output_filename = arguments.output

    discard_points = arguments.discard

    temperature_plot_filename = arguments.temperature
    c_v_plot_filename = arguments.c_v
    c_w_tot_plot_filename = arguments.c_w_tot
    
    #Read a CASTEP MD file and store info in the list
    configuration = md.read_md_file(input_filename)
    if output_filename == "-" or output_filename is None:
        print ('Success - read configuration OK from ',input_filename)
    else:
        with open(output_filename, "w") as file:
            file.write('Success - read configuration OK')


    # Number of configurations
    n_tot_configuration = len(configuration)
    print ('Total number of configurations are: ', n_tot_configuration)

    print ('Number of configurations to discard are: ', discard_points)

    n_configuration = n_tot_configuration - discard_points
    print ('Configurations working with are : ', n_configuration)

    # Number of species in each configurations
    print('Number of species are : ', len(configuration[0].types_of_element))

    # Number of elements present in each configuration
    print('Elements present are: ', configuration[0].types_of_element)

    # Number of atoms for each species
    element_counter = 0
    n_N_atoms = 0
    for _, t in configuration[0].body.items():
        n_N_atoms = len(t)
        print ('Species ', element_counter, ' number of atoms are :', n_N_atoms)
        element_counter = element_counter + 1

    # Total number of atoms in each configurations for all species
    n_atoms = 0
    for _, t in configuration[0].body.items():
        n_atoms += len(t)
    print ('Total number of atoms in crystal are :', n_atoms)

    # Calculate time step
    t_step = float(configuration[1].time)
    print ('Time step value is: ', t_step, 'aut')


    # Set up a 3D array with correct sizes
    velocities = np.zeros ((3, n_atoms, n_tot_configuration), dtype=float)
    masses = np.zeros ((n_atoms))
    temperature = np.zeros ((n_tot_configuration), dtype=float)

    # Create loop over all configurations
    # config_number = time_step
    for config_number in range (discard_points, n_tot_configuration, 1):
        # Number of the atom within the species 
        atom_value = 0
        temperature[config_number] = ((configuration[config_number].temperature))

        # Create loop over all atoms in a configuration
        for _, value in configuration[config_number].body.items(): # Set of velocity data from md file
                    for atom in value:
                        # Set velocity components (x,y,z) of each atom to velocities array
                        # Velocity components are in Angstroms / Femtoseconds

                        # Velocities alreqadyi in aut
                        velocities[0,atom_value,config_number] = ((atom.v_x))
                        velocities[1,atom_value,config_number] = ((atom.v_y))
                        velocities[2,atom_value,config_number] = ((atom.v_z))
                        # Set mass value of each atom to masses array
                        masses[atom_value] = (atom.m)
                        # Atom_value counter increase, to move from one atom to next one
                        atom_value = atom_value + 1

    # Calculate average temperature
    T_average = sum(temperature) / (n_configuration)
    print ('Average temperature is :' , T_average, 'Hartree')
    
    print("")
    
    # Set array for data manipulations and calculations
    c_v_t = np.zeros((n_tot_configuration))
    c_w_tot = np.zeros ((n_tot_configuration),dtype=np.complex_)
    V1 = np.zeros (((n_tot_configuration)))
    VAF2 = np.zeros((n_tot_configuration)*2 - 1)

    # Calculation of c_v_t : Eq. B11
    for i in range(n_atoms):
        for j in range(3):
            for iter in range (n_configuration):
                V1[iter] = velocities[j,i,iter]
            VAF2 = scipy.signal.correlate(V1, V1, mode='full')
            # Two-sided VAF
            VAF2 /=  np.sum(velocities**2)
            c_v_t = VAF2[int(VAF2.size/2):]

            # Fourier transform of c_v_t to obtain c_w : Eq. B10
            # all SI in aut = hbar and boltzman = 1
            c_w = fft(c_v_t)
            
            # Obtain Eq. B12
            c_w_tot += c_w * masses[i] # capital c omega

    # Calculate g(w) = Eq. B12 using c_w_tot
    # Loop over to obtain converted time and temperature values for each configuration

    # N of calculation steps for w = n_configuration
    # Array for f_n
    f_n = np.zeros ((n_tot_configuration))
    # Calculation of allowed frequencies
    for f_count in range (int(n_configuration/2) , int(-n_configuration/2) , -1):
        f_n[f_count] = f_count / (n_configuration * t_step)
    
    print ("")
    print ('This is f_n: ' , f_n)
    
    # Critical frequency
    f_c = 1.0 / (2.0 * float(t_step))
    print ("Critical frequency is: " , f_c)

    # Array for w
    w = np.zeros((n_tot_configuration))
    # CAlculation for allowed values of omega (w)
    for w_index in range ( discard_points, n_tot_configuration , 1):
        w [w_index] = (2 * np.pi) * f_n[w_index]
    print ('This is w: ' , w)
    print ("")

    # Set array for g_w
    g_w = np.zeros((n_tot_configuration),dtype=np.complex_)
    # Calculate g_w
    for g_w_counter in range ( discard_points, n_tot_configuration , 1):
        g_w [g_w_counter] = c_w_tot [g_w_counter] * np.exp( w[g_w_counter] / T_average )



    # Optional plot and data file if temperature flag is given
    if temperature_plot_filename != None:
        
        for T_convert in range (discard_points, n_configuration , 1):
            temperature [T_convert]  = md.atomic_temperature(temperature[T_convert])

        temperature_output = open(temperature_plot_filename, "w")
        np.savetxt(temperature_output, temperature)
        temperature_output.close()

        # Add title and labels for axis
        plt.figure(1)
        plt.xlabel ('Configuration number')
        plt.ylabel ('Temperature in K')
        plt.title ("Temperature vs configurations number")
        # Modify plot and show graph
        plt.plot (temperature, color='red', marker='x', linestyle='dashed', linewidth=2, markersize=0, label = "Data")

    # Optional plot and data file if c_v flag is given
    if c_v_plot_filename != None:
        
        for c_v_convert in range (discard_points, n_configuration , 1):
            temperature [c_v_convert]  = md.atomic_velocity(c_v_t[c_v_convert])

        c_v_output = open(c_v_plot_filename, "w")
        np.savetxt(c_v_output, c_v_t)
        c_v_output.close()

        # Add title and labels for axis
        plt.figure(1)
        plt.xlabel ('Configuration number')
        plt.ylabel ('Velocity in Ang/fs')
        plt.title ("Velocity autocorrellation function")
        # Modify plot and show graph
        plt.plot (c_v_t, color='black', marker='x', linestyle='dashed', linewidth=2, markersize=0, label = "Data")

    # Optional plot and output data file if c_w_tot flag is given
    if c_w_tot_plot_filename != None:

        c_w_tot_output = open(c_w_tot_plot_filename, "w")
        np.savetxt(c_w_tot_output, c_w_tot)
        c_w_tot_output.close()

        # Add title and labels for axis
        plt.figure(2)
        plt.xlabel ('Configuration number')
        plt.ylabel ('Abritrary units')
        plt.title ("Projected occupied phonon DOS")
        # Modify plot and show graph
        plt.plot (abs(c_w_tot), color='blue', marker='x', linestyle='dashed', linewidth=2, markersize=0, label = "Data")
        plt.axis ([-50,2000,-5,80]) # Only half a section of graph shown since is symmetric

    # Output data
    g_w_output = open("g_w_output.txt", "w")
    np.savetxt(g_w_output, g_w)
    g_w_output.close()

    # Mandatory plot for g_w
    # Add title and labels for axis
    plt.figure(3)
    plt.xlabel ('Frequencies / $cm^{-1}$')
    plt.ylabel ('Phonon density of states / arb. units')
    plt.title ("Vibrational spectrum from FFT of $c_{v}(t)$")
    # Modify plot and show graph
    plt.plot (abs(g_w), color='green', marker='x', linestyle='dashed', linewidth=2, markersize=0, label = "Data")
    plt.legend (loc ='upper right')

    plt.show()


if __name__ == "__main__":
    main(sys.argv[1:])

